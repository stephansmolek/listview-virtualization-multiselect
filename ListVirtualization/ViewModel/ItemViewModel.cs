﻿using GalaSoft.MvvmLight;

namespace ListVirtualization.ViewModel
{
    public class ItemViewModel : ViewModelBase
    {
        private string description;

        public ItemViewModel(string description)
        {
            Description = description;
        }

        public string Description
        {
            get => description;
            set => Set(nameof(Description), ref description, value);
        }
    }
}
