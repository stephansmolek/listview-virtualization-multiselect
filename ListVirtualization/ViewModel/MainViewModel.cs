﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using ListVirtualization.Model;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace ListVirtualization.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService dataService;
        private readonly ObservableCollection<ItemViewModel> listItems;
        private ObservableCollection<ItemViewModel> selectedListItems;

        private RelayCommand<SelectionChangedEventArgs> selectedItemsChangedCommand;

        public MainViewModel(IDataService dataService)
        {
            listItems = new ObservableCollection<ItemViewModel>();
            selectedListItems = new ObservableCollection<ItemViewModel>();

            SelectedItemsChangedCommand = new RelayCommand<SelectionChangedEventArgs>(e => selectedItemsChanged(e));

            this.dataService = dataService;
            this.dataService.GetData(
                (data, error) =>
                {
                    if (error != null)
                    {
                        return;
                    }

                    data.List.ForEach(item => ListItems.Add(new ItemViewModel(item)));
                    RaisePropertyChanged(nameof(ListItems));
                });
        }
        
        public ObservableCollection<ItemViewModel> ListItems { get => listItems; }

        public ObservableCollection<ItemViewModel> SelectedListItems { get => selectedListItems; }

        public int NumberOfSelectedItems
        {
            get => SelectedListItems.Count;
        }


        public RelayCommand<SelectionChangedEventArgs> SelectedItemsChangedCommand
        {
            get => selectedItemsChangedCommand;
            set => Set(nameof(SelectedItemsChangedCommand), ref selectedItemsChangedCommand, value);
        }


        private void selectedItemsChanged(SelectionChangedEventArgs e)
        {
            Set(nameof(SelectedListItems), ref selectedListItems, new ObservableCollection<ItemViewModel>(selectedListItems
                .Except(e.RemovedItems.Cast<ItemViewModel>())
                .Concat(e.AddedItems.Cast<ItemViewModel>())));
            RaisePropertyChanged(nameof(NumberOfSelectedItems));
        }
    }
}