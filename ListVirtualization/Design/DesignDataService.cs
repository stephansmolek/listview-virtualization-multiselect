﻿using System;
using ListVirtualization.Model;

namespace ListVirtualization.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<Data, Exception> callback)
        {
            Data data = new Data();
            callback(data, null);
        }
    }
}