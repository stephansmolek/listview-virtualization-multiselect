﻿using System.Collections.Generic;

namespace ListVirtualization.Model
{
    public class Data
    {
        private const int numberOfItems = 1_000_000;

        private List<string> list;

        public Data()
        {
            initiateList();
        }

        public List<string> List
        {
            get => list;
        }

        private void initiateList()
        {
            list = new List<string>(numberOfItems);

            for (int i = 0; i < numberOfItems; i++)
                list.Add($"This is item number {i + 1}.");
        }
    }
}