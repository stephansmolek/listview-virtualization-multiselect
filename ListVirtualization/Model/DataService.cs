﻿using System;

namespace ListVirtualization.Model
{
    public class DataService : IDataService
    {
        public void GetData(Action<Data, Exception> callback)
        {
            Data data = new Data();
            callback(data, null);
        }
    }
}