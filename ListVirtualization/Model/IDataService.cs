﻿using System;

namespace ListVirtualization.Model
{
    public interface IDataService
    {
        void GetData(Action<Data, Exception> callback);
    }
}
